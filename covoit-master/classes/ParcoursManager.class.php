<?php
class ParcoursManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function insertParcours($ville1,$ville2,$km){
		if($this->existe($ville1, $ville2)){
			return false;
		}else{
			$req = $this->db->prepare('INSERT into parcours(vil_num1,vil_num2,par_km)values(:ville1, :ville2, :km)');
			$req->bindValue(':ville1',$ville1);
			$req->bindValue(':ville2',$ville2);
			$req->bindValue(':km',$km);
			$bon = $req->execute();
			return $bon;
		}
	}

	public function existe($ville1, $ville2){
		$req = $this->db->prepare('SELECT vil_num1 from parcours
															where (vil_num1=:ville1 and vil_num2=:ville2)
															or (vil_num1=:ville2 and vil_num2=:ville1)');
		$req->bindValue(':ville1',$ville1);
		$req->bindValue(':ville2',$ville2);
		$bon = $req->execute();
		if(empty($req->fetch(PDO::FETCH_OBJ))){
			return false;
		}else{
			return true;
		}
	}

	public function nbrParcours(){
		$req = $this->db->query('SELECT COUNT(par_num) as nbr from parcours');
		return $req->fetch(PDO::FETCH_OBJ)->nbr;
	}

	public function getParcours(){
		$parcours = array();
		$req = $this->db->query('SELECT par_num, par_km, vil_num1, vil_num2 FROM parcours');
		while ($value = $req->fetch(PDO::FETCH_OBJ)) {
			$parcours[] = new Parcours($value);
		}
		return $parcours;
	}

	public function getParcoursPrecis($id){
		$req = $this->db->prepare('SELECT par_num, par_km, vil_num1, vil_num2 FROM parcours where par_num=:id');
		$req->bindValue(':id', $id);
		$req->execute();
		$parcours = new Parcours($req->fetch());
		return $parcours;
	}

	public function getParcoursParVille($vil_num1, $vil_num2){
		$sql = 'SELECT * FROM parcours
						where (vil_num1 = :vil_num1 and vil_num2 = :vil_num2) or (vil_num1 = :vil_num2 and vil_num2 = :vil_num1)';
		$req = $this->db->prepare($sql);
		$req->bindValue(':vil_num1', $vil_num1);
		$req->bindValue(':vil_num2',$vil_num2);
		$req->execute();
		$parcours = new Parcours($req->fetch());
		return $parcours;
	}

	public function getVillesDansParcours(){
		$villes = array();
		$sql = 'SELECT DISTINCT vil_num1 as vil_num, vil_nom
						from parcours p join ville v on v.vil_num=p.vil_num1
						union SELECT DISTINCT vil_num2 as vil_num, vil_nom
						from parcours p join ville v on v.vil_num=p.vil_num2';
		$req = $this->db->query($sql);

		while($value = $req->fetch(PDO::FETCH_OBJ)){
			$villes[] = new Ville($value);
		}
		return $villes;
	}

	public function getSens($idParcours, $vil_NumDep){
		$req = $this->db->prepare('SELECT vil_num1 FROM parcours where par_num= :par_num');
		$req->bindValue(':par_num', $idParcours);
		$req->execute();
		$vil_Num1 = $req->fetch(PDO::FETCH_OBJ)->vil_num1;

		//allée
		if($vil_num1=$vil_NumDep){
			return 0;
		//retour
		}else{
			return 1;
		}
	}
}
