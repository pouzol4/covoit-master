<?php
class EtudiantManager extends Personne{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function insertEtudiant($post){
		$req = $this->db->prepare('INSERT INTO etudiant(per_num,dep_num,div_num) VALUES(:idPers, :idDep, :idDiv)');
		$req->bindValue(':idPers', $post['personne']);
		$req->bindValue(':idDep', $post['departement']);
		$req->bindValue(':idDiv', $post['division']);
		$req->execute();
	}

}
