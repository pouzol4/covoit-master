<?php
class VilleManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function insererVille($nom){
		if($this->existe($nom)){
			return false;
		}else{
			$req= $this->db->prepare('INSERT INTO ville(vil_nom)values(:nom)');
			$req->bindValue(':nom',$nom);
			$bon = $req->execute();
			return $bon;
		}
	}

	public function existe($nom){
		$req= $this->db->prepare('SELECT vil_nom from ville where vil_nom=:nom');
		$req->bindValue(':nom',$nom);
		$bon = $req->execute();
		if(empty($req->fetch(PDO::FETCH_OBJ))){
			return false;
		}else{
			return true;
		}
	}

	public function compteVille(){
		$req = $this->db->query('SELECT COUNT(vil_num) as nbr from ville');
		$ville = $req->fetch(PDO::FETCH_OBJ);
		return $ville->nbr;
	}

	public function getVilles(){
		$villes = array();
		$req = $this->db->query('SELECT vil_num, vil_nom FROM ville ORDER BY vil_nom');

		while ($ville = $req->fetch(PDO::FETCH_OBJ)) {
			$villes[] = new Ville($ville);
		}
		return $villes;
	}

	public function getVille($idVille){
		$req = $this->db->prepare('SELECT vil_num, vil_nom FROM ville where vil_num = :id');
		$req->bindValue(':id',$idVille);
		$req->execute();
		return new Ville($req->fetch(PDO::FETCH_OBJ));
	}
}
