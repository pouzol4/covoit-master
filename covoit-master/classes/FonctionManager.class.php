<?php
class FonctionManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function getFonctions(){
		$listeFonction = array();
		$req = $this->db->query('SELECT fon_num, fon_libelle from fonction');

		while ($fonction = $req->fetch(PDO::FETCH_OBJ)) {
			$listeFonction[] = new Fonction($fonction);
		}
		$req->closeCursor();
		return $listeFonction;
	}

	public function getFonction($fon_num){
		$req = $this->db->prepare('SELECT fon_num, fon_libelle from fonction where fon_num=:idFonction');
		$req->bindValue(':idFonction', $fon_num);
		$req->execute();
		return new Fonction($req->fetch(PDO::FETCH_OBJ));
	}

}
