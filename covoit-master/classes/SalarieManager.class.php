<?php
class SalarieManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function insertSalarie($post){
		$req = $this->db->prepare('INSERT INTO salarie(per_num,sal_telprof,fon_num) VALUES(:idPers, :numPro, :idFon)');
		$req->bindValue(':idPers', $post['personne']);
		$req->bindValue(':numPro', $post['telephone']);
		$req->bindValue(':idFon', $post['fonction']);
		$req->execute();
	}

	public function getSalarie($idPersonne){
		$req = $this->db->prepare('SELECT per_num, sal_telprof, fon_num from salarie where per_num=:idPersonne');
		$req->bindValue(':idPersonne', $idPersonne);
		$req->execute();
		return new Salarie($req->fetch(PDO::FETCH_OBJ));
	}

}
