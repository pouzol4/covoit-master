<?php
class Etudiant{
	private $per_num;
	private $dep_num;
	private $div_num;

	function __construct($valeurs = array()){
			if(!empty($valeurs)){
				$this->affecte($valeurs);
			}
		}

		private function affecte($donnees){
			foreach ($donnees as $attribut => $valeur) {
				switch ($attribut) {
					case 'per_num': $this->per_num = $valeur; break;
					case 'dep_num': $this->dep_num = $valeur; break;
					case 'div_num': $this->div_num = $valeur; break;
				}
			}
		}

}
