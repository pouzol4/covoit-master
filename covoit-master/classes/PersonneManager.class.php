<?php
class PersonneManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function insertPersonne($post){
		$sql = 'INSERT INTO personne(per_nom,per_prenom,per_tel,per_mail,per_login,per_pwd) VALUES(:nom, :prenom, :tel, :mail, :login, :pwd)';
		$req = $this->db->prepare($sql);
		$req->bindValue(':nom', $post['Nom']);
		$req->bindValue(':prenom', $post['Prenom']);
		$req->bindValue(':tel', $post['Telephone']);
		$req->bindValue(':mail', $post['Email']);
		$req->bindValue(':login', $post['Login']);
		$password = sha1(sha1($post['motDePasse']).SALT);
		$req->bindValue(':pwd', $password);
		$req->execute();
	}

	public function comptePersonne(){
		$req = $this->db->query('SELECT COUNT(per_num) as nbr from personne');
		return $req->fetch(PDO::FETCH_OBJ)->nbr;
	}

	public function getPersonnes(){
		$personnes = array();
		$req = $this->db->query('SELECT per_num,per_nom,per_prenom,per_tel,per_mail,per_login,per_pwd
														FROM personne');

		while ($personne = $req->fetch(PDO::FETCH_OBJ)) {
			$personnes[] = new Personne($personne);
		}
		return $personnes;
	}

	public function getPersonne($id){
		$req = $this->db->prepare('SELECT per_num,per_nom,per_prenom,per_tel,per_mail,per_login,per_pwd
															 FROM personne where per_num=:id');
		$req->bindValue(':id',$id);
		$req->execute();
		$personne = new Personne($req->fetch(PDO::FETCH_OBJ));
		return $personne;
	}

	public function getPersonneConnecte($post){
		$req = $this->db->prepare('SELECT per_num,per_nom FROM personne where per_login=:login and per_pwd=:pwd');
		$req->bindValue(':login',$post['nomUser']);

		$password = sha1(sha1($post['passwd']).SALT);
		$req->bindValue(':pwd', $password);

		$req->execute();
		$personne = new Personne($req->fetch(PDO::FETCH_OBJ));
		return $personne;
	}

	public function getMoyenneAvis($id){
		$req = $this->db->prepare('SELECT avg(avi_note) as moyenne from avis where per_per_num= :idPersonne');
		$req->bindValue(':idPersonne',$id);
		$req->execute();
		$moyenne = $req->fetch(PDO::FETCH_OBJ)->moyenne;
		return $moyenne;
	}

	public function getDernierAvis($id){
		$sql = 'SELECT avi_comm from avis where per_per_num= :idPersonne order by avi_date desc';
		$req = $this->db->prepare($sql);
		$req->bindValue(':idPersonne',$id);
		$req->execute();
		$dernierAvis = $req->fetch(PDO::FETCH_ASSOC)['avi_comm'];
		return $dernierAvis;
	}

	public function isSalarie($idPersonne){
		$req= $this->db->prepare('SELECT per_num from salarie where per_num= :id');
		$req->bindValue(':id',$idPersonne);
		$req->execute();
		return $req->fetch(PDO::FETCH_OBJ);
	}

	public function isEtudiant($idPersonne){
		$req= $this->db->prepare('SELECT per_num from etudiant where per_num= :id');
		$req->bindValue(':id',$idPersonne);
		$req->execute();

		return $req->fetch(PDO::FETCH_OBJ);
	}

	public function modiferPersonne($personne){
		$sql = 'UPDATE personne SET per_nom = :nom, per_prenom = :prenom,
						per_tel = :tel, per_mail = :mail, per_login = :login,
						per_pwd= :pwd where per_num = :id';
		$req = $this->db->prepare($sql);

		$req->bindValue(':nom', $personne->getNom());
		$req->bindValue(':prenom', $personne->getPrenom());
		$req->bindValue(':tel', $personne->getTel());
		$req->bindValue(':mail', $personne->getMail());
		$req->bindValue(':login', $personne->getLogin());
		$req->bindValue(':pwd', sha1(sha1($personne->getPwd()).SALT));
		$req->bindValue(':id', $personne->getNum());
		$req->execute();
	}

	public function loginPasswordBon($post){
		$req = $this->db->prepare('SELECT per_num from personne where per_login= :login and per_pwd= :pwd');
		$req->bindValue(':login', $post['nomUser']);

		$password = sha1(sha1($post['passwd']).SALT);
		$req->bindValue(':pwd', $password);

		$req->execute();
		return $req->fetch(PDO::FETCH_OBJ);
	}

	public function dropPersonne($personneId){
		if($this->isEtudiant($personneId)){
			$this->dropEtudiant($personneId);
		}

		if($this->isSalarie($personneId)){
			$this->dropSalarie($personneId);
		}

		$this->dropAvis($personneId);
		$db = new Mypdo();
		$proposeManager = new ProposeManager($db);
		$proposeManager->dropPropose($personneId);

		$sql = 'DELETE FROM personne where per_num = :id';
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $personneId);
		$bon = $req->execute();
		return $bon;
	}

	public function dropEtudiant($personneId){
		$sql = 'DELETE FROM etudiant where per_num = :id';
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $personneId);
		$bon = $req->execute();
	}

	public function dropAvis($personneId){
		$sql = 'DELETE FROM avis where per_num = :id or per_per_num = :id';
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $personneId);
		$bon = $req->execute();
	}

	public function dropSalarie($personneId){
		$sql = 'DELETE FROM salarie where per_num = :id';
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $personneId);
		$bon = $req->execute();
	}

}
