<?php //A COMPLETER
class DepartementManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function getDepartements(){
		$listeDepartement = array();
		$req = $this->db->query('SELECT * from departement');

		while ($departement = $req->fetch(PDO::FETCH_OBJ)) {
			$listeDepartement[] = new Departement($departement);
		}
		$req->closeCursor();
		return $listeDepartement;
	}

	public function getDepartementPersonne($numeroPersonne){
		$sql = 'SELECT d.dep_num, dep_nom, vil_num from departement d join etudiant e on d.dep_num=e.dep_num WHERE per_num=:idPersonne';
		$req = $this->db->prepare($sql);
		$req->bindValue(':idPersonne', $numeroPersonne);
		$req->execute();
		return new Departement($req->fetch(PDO::FETCH_OBJ));
	}
}
