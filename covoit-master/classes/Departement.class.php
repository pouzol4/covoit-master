<?php
class Departement{
  private $dep_num;
  private $dep_nom;
  private $vil_num;

  function __construct($valeurs = array()){
      if(!empty($valeurs)){
        $this->affecte($valeurs);
      }
    }

    private function affecte($donnees){
      foreach ($donnees as $attribut => $valeur) {
        switch ($attribut) {
          case 'dep_num': $this->dep_num = $valeur; break;
          case 'dep_nom': $this->dep_nom = $valeur; break;
          case 'vil_num': $this->vil_num = $valeur; break;
        }
      }
    }

    public function getId(){
      return $this->dep_num;
    }

    public function getNom(){
      return $this->dep_nom;
    }

    public function getNomVille(){
      $db = new Mypdo();
      $villeManager = New VilleManager($db);
      return $villeManager->getVille($this->vil_num)->getNom();
    }
}
