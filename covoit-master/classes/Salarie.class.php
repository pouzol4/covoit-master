<?php
class Salarie extends Personne{
	private $per_num;
	private $sal_telprof;
	private $fon_num;

	function __construct($valeurs = array()){
			if(!empty($valeurs)){
				$this->affecte($valeurs);
			}
		}

		private function affecte($donnees){
			foreach ($donnees as $attribut => $valeur) {
				switch ($attribut) {
					case 'per_num': $this->per_num = $valeur; break;
					case 'sal_telprof': $this->sal_telprof = $valeur; break;
					case 'fon_num': $this->fon_num = $valeur; break;
				}
			}
		}

		public function getTelPro(){
			return $this->sal_telprof;
		}

		public function getFonction(){
		  $db = new Mypdo();
	  	$fonctionManager = new FonctionManager($db);
			return $fonctionManager->getFonction($this->fon_num)->getLibelle();
		}

}
