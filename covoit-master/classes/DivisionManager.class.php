<?php //A COMPLETER
class DivisionManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function getDivision(){
		$listeDivision = array();
		$req = $this->db->query('SELECT div_num, div_nom from division');
		while ($division = $req->fetch(PDO::FETCH_OBJ)) {
			$listeDivision[] = new Division($division);
		}
		$req->closeCursor();
		return $listeDivision;
	}
}
