<?php
class ProposeManager{
	private $db;

	function __construct($db){
		$this->db = $db;
	}

	public function ajouterParcours($post){
	  $db = new Mypdo();
	  $parcoursManager = New ParcoursManager($db);

		$parcours = $parcoursManager->getParcoursParVille($post['villeDepart'], $post['villeArrivee']);
		$sens = $parcoursManager->getSens($parcours->getNum(), $post['villeDepart']);
		$sql = 'INSERT INTO propose(par_num, per_num, pro_date, pro_time, pro_place, pro_sens) values(:par_num, :per_num, :pro_date, :pro_time, :pro_place, :pro_sens)';
		$req = $this->db->prepare($sql);
		$req->bindValue(':par_num', $parcours->getNum());
		$req->bindValue('per_num', $_SESSION['idUser']);
		$req->bindValue(':pro_date', $post['dateDepart']);
		$req->bindValue(':pro_time', $post['heureDepart']);
		$req->bindValue(':pro_place', $post['NbrPlace']);
		$req->bindValue(':pro_sens', $sens);
		$bon = $req->execute();
		return $bon;
	}

	public function getVilleArrivee($idVille){
		$villesArrivee = array();

		$sql = 'SELECT DISTINCT vil_num2 as vil_num, vil_nom from parcours p
						join ville v on v.vil_num=p.vil_num2 where vil_num1 = :vilnum
						UNION SELECT DISTINCT vil_num1 as vil_num, vil_nom from parcours p
						join ville v on v.vil_num=p.vil_num1 where vil_num2 = :vilnum';
		$req = $this->db->prepare($sql);
		$req->bindValue(':vilnum', $idVille);
		$req->execute();

		while ($ville = $req->fetch(PDO::FETCH_OBJ)) {
			$villesArrivee[] = new Ville($ville);
		}
		return $villesArrivee;
	}

	public function getVilles(){
		$villes = array();
		$sql = 'SELECT DISTINCT vil_num1 as vil_num, vil_nom
						from parcours p join ville v on v.vil_num=p.vil_num1 join propose pr on pr.par_num = p.par_num
						where pr.pro_sens = 0
						union SELECT DISTINCT vil_num2 as vil_num, vil_nom
						from parcours p join ville v on v.vil_num=p.vil_num2 join propose pr on pr.par_num = p.par_num
						where pr.pro_sens= 1';
		$req = $this->db->query($sql);

		while($value = $req->fetch(PDO::FETCH_OBJ)){
			$villes[] = new Ville($value);
		}
		return $villes;
	}

	public function getTrajetTrouve($villeDepart, $villeArrivee, $DateDebut, $DateFin, $heureDebut){
		$trajetTrouve = array();
		$sql = "SELECT pr.par_num, per_num, pro_date, pro_time, pro_place
						from parcours p join propose pr on p.par_num=pr.par_num
						where vil_num1=:villeDeb and vil_num2=:villeAriv and pro_sens=0
						and pro_date>=:dateDeb and pro_date <= :dateFin  and pro_time >= :heureDeb

						UNION SELECT pr.par_num, per_num, pro_date, pro_time, pro_place
						from parcours p join propose pr on p.par_num=pr.par_num
						where vil_num2=:villeDeb and vil_num1=:villeAriv and pro_sens=1
						and pro_date>=:dateDeb and pro_date <= :dateFin  and pro_time >= :heureDeb";

		$req = $this->db->prepare($sql);
		$req->bindValue(':villeDeb', $villeDepart);
		$req->bindValue(':villeAriv', $villeArrivee);
		$req->bindValue(':dateDeb', $DateDebut);
		$req->bindValue(':dateFin', $DateFin);
		$req->bindValue(':heureDeb', $heureDebut);
		$req->execute();

		while ($value = $req->fetch(PDO::FETCH_OBJ)) {
			$trajetTrouve[]=new Propose($value);
		}
		return $trajetTrouve;
	}


	public function dropPropose($personneId){
		$sql = 'DELETE FROM propose where per_num = :id';
		$req = $this->db->prepare($sql);
		$req->bindValue(':id', $personneId);
		$bon = $req->execute();
	}
}
