<?php         if (!(empty($_POST['modifier']))) {
                $personne = new Personne($_POST);
                $personneManager->modiferPersonne($personne);
} ?>
              <h1>Modifier Personne</h1>
<?php
if(empty($_POST['idPersonne'])){
  $personnes = $personneManager->getPersonnes();
  if(count($personnes)==0){?>
              <img src="image/erreur.png" alt="erreur"><span>Désolé plus de personnes à supprimer !</span>
<?php
  }else{?>
              <form class="" action="#" method="post">
                <select class="" name="idPersonne">
<?php            foreach ($personnes as $personne) {?>
                    <option value="<?php echo $personne->getNum() ?>"><?php echo $personne->getLogin() ?></option>
<?php             } ?>
                  </select>
                  <input type="submit" name="modifier" value="Modifier">
                </form>
<?php
  }
}else{
            $personne = $personneManager->getPersonne($_POST['idPersonne'])?>
              <form class="modifierPersonne" action="#" method="post">
                <fieldset>
                  <label for="Nom">Nom : </label>
      						<input type="text" name="per_nom" id="Nom" value="<?php echo $personne->getNom() ?>" required />

      						<label for="Prenom">Prenom : </label>
      						<input type="text" name="per_prenom" id="Prenom" value="<?php echo $personne->getPrenom() ?>" required />

      						<label for="Telephone">Téléphone :</label>
      						<input type="tel" name="per_tel" id="Telephone" value="<?php echo $personne->getTel() ?>" required />

      						<label for="Email">Mail :</label>
      						<input type="email" name="per_mail" id="Email" value="<?php echo $personne->getMail() ?>" required />

      						<label for="Login">Login :</label>
      						<input type="text" name="per_login" id="Login" value="<?php echo $personne->getLogin() ?>" required />

      						<label for="motDePasse">Mot de passe :</label>
      						<input type="password" name="per_pwd" id="motDePasse" required />
                </fieldset>

                <input type="hidden" name="per_num" value="<?php echo $_POST['idPersonne'] ?>">
                <input type="submit" name="modifier" value="Modifier">
              </form>
<?php }
