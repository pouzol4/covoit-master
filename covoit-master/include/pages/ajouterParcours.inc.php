              <h1>Ajouter un parcours</h1>
<?php         if(empty($_POST['nbrKilomètre']) || $_POST['ville1'] == $_POST['ville2'] || $_POST['nbrKilomètre']<=0){
                $villes = $villeManager->getVilles();?>

                <form class="" action="#" method="post">
                  <label for="ville1">Ville 1:</label>
                  <select class="" name="ville1" id="ville1">
<?php               foreach ($villes as $value){ ?>
                      <option value="<?php echo $value->getNum() ?>"><?php echo $value->getNom() ?></option>
<?php               } ?>
                  </select>

                  <label for="ville2">Ville 2:</label>
                  <select class="" name="ville2" id="ville2">
<?php               foreach ($villes as $value){ ?>
                      <option value="<?php echo $value->getNum() ?>"><?php echo $value->getNom() ?></option>
<?php               } ?>
                  </select>

                  <label for="nbrKilomètre">Nombre de kilomètre(s)</label>
                  <input type="number" name="nbrKilomètre"id="nbrKilomètre" /> <br /> <br />
                  <input type="submit" name="valider" value="Valider" class="valider" />
                </form>
<?php         }else{
                if($parcoursManager->insertParcours($_POST['ville1'],$_POST['ville2'],$_POST['nbrKilomètre'])){?>
              <img src="image/valid.png" alt="valider">
              <span> Le parcours a été ajoutée</span>
<?php          }else{?>
              <img src="image/erreur.png" alt="erreur">
              <span> Le parcours n'a pas pu être ajoutée</span>
<?php          }
              } ?>
