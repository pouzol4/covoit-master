				<h1>Ajouter une personne</h1>
<?php
if(!(empty($_POST['departement']) || empty($_POST['division']))){
	$etudiantManager->insertEtudiant($_POST);
}

if(!(empty($_POST['telephone']) || empty($_POST['fonction']))){
	$salarieManager->insertSalarie($_POST);
}

if(empty($_POST['Nom']) || empty($_POST['Prenom']) || empty($_POST['Telephone']) || empty($_POST['Email']) || empty($_POST['Login']) || empty($_POST['motDePasse']) || empty($_POST['categorie'])) { ?>
				<form class="ajouterPersonne" action="#" method="post">
					<fieldset>
						<label for="Nom">Nom : </label>
						<input type="text" name="Nom" id="Nom" required />

						<label for="Prenom">Prenom : </label>
						<input type="text" name="Prenom" id="Prenom" required />

						<label for="Telephone">Téléphone :</label>
						<input type="tel" pattern="(01|02|03|04|05|06|07|08|09)[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}[ \.\-]?[0-9]{2}" name="Telephone" id="Telephone" required />

						<label for="Email">Mail :</label>
						 <input type="email" pattern="[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([_\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})" name="Email" id="Email" required />

						<label for="Login">Login :</label>
						 <input type="text" name="Login" id="Login" required />

						<label for="motDePasse">Mot de passe :</label>
						 <input type="password" name="motDePasse" id="motDePasse" required />
					</fieldset>

					<label>Catégorie</label>
					<input type="radio" name="categorie" value="Etudiant" checked /> Etudiant
					<input type="radio" name="categorie" value="Personnel" /> Personnel <br />

					<input type="submit" name="Valider" value="Valider" class="valider" />
				</form>
<?php }else{
				$personneManager->insertPersonne($_POST);
				if($_POST['categorie']=='Etudiant'){?>
				<form class="" action="#" method="post">
					<input type="hidden" name="personne" value="<?php echo $db->lastInsertId(); ?>">

					<label for="division">Année </label>
					<select class="" name="division" id="division">
<?php 			$divisions = $divisionManager->getDivision();
					foreach ($divisions as $value) {?>
						<option value="<?php echo $value->getId() ?>"><?php echo $value->getNom() ?></option>
<?php	} ?>
					</select> <br />

					<label for="departement">Département</label>
					<select class="" name="departement" id="departement">
<?php				$departements = $departementManager->getDepartements();
					foreach ($departements as $value) {?>
						<option value="<?php echo $value->getId() ?>"><?php echo $value->getNom().' ('.$value->getNomVille().')' ?></option>
<?php	} ?>
					</select> <br />
					<input type="submit" name="valider" value="valider" class="valider" />
				</form>
<?php
	}
				if($_POST['categorie']=='Personnel'){?>
				<form class="" action="#" method="post">
					<input type="hidden" name="personne" value="<?php echo $db->lastInsertId(); ?>">
					<label for="telephone">Téléphone professionnel :</label>
					<input type="tel" name="telephone" id="telephone" required> <br />

					<label for="fonction">Fonction </label>
					<select class="" name="fonction" id="fonction">
<?php			$fonctions = $fonctionManager->getFonctions();
					foreach ($fonctions as $value) {?>
						<option value="<?php echo $value->getId() ?>"><?php echo $value->getLibelle() ?></option>
<?php	} ?>
					</select> <br />

					<input type="submit" name="valider" value="valider" class="valider" />
				</form>
<?php	}
} ?>
