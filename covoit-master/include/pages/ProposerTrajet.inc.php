<?php if (empty($_SESSION['loginUser'])) { ?>
              <script type='text/javascript'>window.location.href='index.php'</script>;
<?php } ?>

              <h1>Proposer un trajet</h1>


<?php         $villeDepart = $parcoursManager->getVillesDansParcours();
                if(empty($_POST['villeDepart'])){ ?>
                <form id="recherche" class="trajetVilleDepart" action="#" method="post">
                  <label for="villeDepart">Ville de départ :</label> <br /> <br />
                  <select name="villeDepart" onChange='javascript:document.getElementById("recherche").submit()'>
                    <option value="0">Choisissez</option>
<?php
                      foreach ($villeDepart as $ville) { ?>
                        <option value="<?php echo $ville->getNum(); ?>"><?php echo $ville->getNom(); ?></option>
<?php                 } ?>
                  </select> <br /> <br />
                </form>
<?php           }else{
                      if(empty($_POST['villeArrivee']) || empty($_POST['dateDepart']) || empty($_POST['heureDepart']) || empty($_POST['NbrPlace'])){
                        $villeDepart = $villeManager->getVille($_POST['villeDepart']);
                        $villeArrivee = $proposeManager->getVilleArrivee($villeDepart->getNum());

                        /*AUJOURD'HUI*/
                        $aujourdHui = date('Y-m-d');

                        /*HEURE ACTUELLE*/
                        $maintenant = date('H:i:s');
              ?>
                <form class="trajetProposer" action="#" method="post">
                  <span>Ville de départ : <?php echo $villeDepart->getNom() ?></span>

                  <label for="villeArrivee">Ville d'arrivée : </label>
                  <select name="villeArrivee" id="villeArrivee"> <?php
                    foreach ($villeArrivee as $ville) { ?>
                      <option value="<?php echo $ville->getNum() ?>"><?php echo $ville->getNom() ?></option>
<?php               } ?>
                  </select> <br />

                  <label for="dateDepart">Date de départ : </label>
                  <input type="date" id="dateDepart" name="dateDepart" value='<?php echo $aujourdHui ?>' min="<?php echo $aujourdHui ?>" />

                  <label for="heureDepart">Heure de départ : </label>
                  <input type="time" id="heureDepart" name="heureDepart" value='<?php echo $maintenant ?>' /> <br />

                  <label for="NbrPlace">Nombre de places : </label>
                  <input type="number" id="NbrPlace" name="NbrPlace" value="1" min="1" required /> <br />

                  <input type="hidden" name="villeDepart" value="<?php echo $_POST['villeDepart'] ?>">
                  <input type="submit" name="valider" value="Valider" class="valider" />
                </form>

<?php         }else{
                  if($proposeManager->ajouterParcours($_POST)){?>
                  <img src="image/valid.png" alt="valider"> Le parcours est proposer aux autres utilisateurs
<?php             }else{?>
                  <img src="image/erreur.png" alt="erreur"> Le parcours n'est pas proposer aux autres utilisateurs
<?php             }
                }
              }  ?>
