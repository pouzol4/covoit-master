<?php if (empty($_SESSION['loginUser'])) { ?>
              <script type='text/javascript'>window.location.href='index.php'</script>;
<?php } ?>

              <h1>Rechercher un trajet</h1>

<?php $villeDepart = $proposeManager->getVilles();
    if(empty($_POST['villeDepart']) && empty($_POST['villeArrivee'])){ ?>
      <form id="recherche" class="trajetVilleDepart" action="#" method="post">
        <label for="villeDepart">Ville de départ :</label> <br /> <br />
        <select id="villeDepart" name="villeDepart" onChange='javascript:document.getElementById("recherche").submit()'>
          <option value="0">Choisissez</option>
<?php
        foreach ($villeDepart as $ville) { ?>
          <option value="<?php echo $ville->getNum(); ?>"><?php echo $ville->getNom(); ?></option>
<?php } ?>
        </select> <br /> <br />
      </form>
<?php }else{
        if (empty($_POST['villeArrivee'])) {
          var_dump($_POST);
          $villeDepart = $villeManager->getVille($_POST['villeDepart']);
          $villesArrivee = $proposeManager->getVilleArrivee($_POST['villeDepart']);

          /*AUJOURD'HUI*/
          $aujourdHui = date('Y-m-d');
        ?>

          <form class="" action="#" method="post">
            <label for="villeDepartSur">Ville de départ:<?php echo $villeDepart->getNom(); ?></label>
            <input id="villeDepartSur" type="hidden" name="villeDepart" value="<?php echo $_POST['villeDepart'] ?>">
            <label for="villeArrivee">Ville d'arrivée : </label>
            <select id="villeArrivee" class="" name="villeArrivee">
        <?php    foreach ($villesArrivee as $value){ ?>
              <option value="<?php echo $value->getNum() ?>"><?php echo $value->getNom() ?></option>
        <?php } ?>
            </select> <br />
            <label for="dateDepart">Date de départ : </label>
            <input id="dateDepart" type="date" name="dateDepart" value="<?php echo $aujourdHui ?>" min="<?php echo $aujourdHui ?>">
            <label for="precision">Précision : </label>
            <select id="precision" class="" name="precision">
              <option value="0">Ce jour</option>
              <option value="1">+/- 1 jour</option>
              <option value="2">+/- 2 jour</option>
              <option value="3">+/- 3 jour</option>
            </select> <br />
            <label for="heureDebut">A partir de : </label>
            <select id="heureDebut" class="" name="heureDebut">
        <?php for ($i=0; $i <= 23 ; $i++) { ?>
              <option value="<?php echo $i ?>:00:00"><?php echo $i."h" ?></option>
        <?php } ?>
            </select> <br />
            <input type="submit" class="valider" name="valider" value="Valider">
          </form>

<?php }else{
  $DateDebut = date('Y-m-d', strtotime($_POST['dateDepart']."-".$_POST['precision'].'days'));
  $DateFin =  date('Y-m-d', strtotime($_POST['dateDepart']."+".$_POST['precision'].'days'));
  $trajetTrouve = $proposeManager->getTrajetTrouve($_POST['villeDepart'], $_POST['villeArrivee'], $DateDebut, $DateFin, $_POST['heureDebut']);
  if(count($trajetTrouve)==0){?>
    <img src="image/erreur.png" alt="erreur"><span>Désolé pas de trajet disponible !</span>
  <?php }else{?>
    <table>
      <tr>
        <th>Ville départ</th>
        <th>Ville arrivée</th>
        <th>Date départ</th>
        <th>Heure départ</th>
        <th>Nombre de place(s)</th>
        <th>Nom du covoitureur</th>
      </tr>
    <?php foreach ($trajetTrouve as $value) {
      $parcours = $parcoursManager->getParcoursPrecis($value->getParcoursNum());
      $ville1 = $villeManager->getVille($parcours->getVille1());
      $ville2 = $villeManager->getVille($parcours->getVille2());
      $personne =$personneManager->getPersonne($value->getPersonneNum());
      $moyenne = $personneManager->getMoyenneAvis($personne->getNum());
      $dernierAvis = $personneManager->getDernierAvis($personne->getNum())?>
      <tr>
        <td><?php echo $ville1->getNom() ?></td>
        <td><?php echo $ville2->getNom() ?></td>
        <td><?php echo $value->getDate() ?></td>
        <td><?php echo $value->getHeure() ?></td>
        <td><?php echo $value->getPlace() ?></td>
        <td title='Moyenne des avis : <?php echo $moyenne ?> Dernier avis : <?php echo $dernierAvis; ?>'><?php echo $personne->getNom()." ".$personne->getPrenom() ?></td>
      </tr>
    <?php } ?>
    </table>

    <?php }?>

<?php }
} ?>
