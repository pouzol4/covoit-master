<?php
  require_once("include/autoLoad.inc.php");
  require_once("include/config.inc.php");

  $db = new Mypdo();
  $villeManager = New VilleManager($db);
  $parcoursManager = New ParcoursManager($db);
  $personneManager = New PersonneManager($db);
  $departementManager = new DepartementManager($db);
  $divisionManager = new DivisionManager($db);
  $etudiantManager = new EtudiantManager($db);
  $fonctionManager = new FonctionManager($db);
  $salarieManager = new SalarieManager($db);
  $proposeManager = new ProposeManager($db);

require_once("include/header.inc.php");

?>
    <div id="corps">
<?php
require_once("include/menu.inc.php");
require_once("include/texte.inc.php");
?>
    </div>

    <div id="spacer"></div>
<?php
require_once("include/footer.inc.php"); ?>
