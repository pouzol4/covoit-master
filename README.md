# Covoit-master

Ce site est une application demandée par Monsieur CHASTAGNER afin de nous apprendre et d'apréhender le language PHP. Ce projet est un site permettant aux utilisateurs de pouvoir demander ou de proposer un covoiturages.

## Consigne

Voici où vous pouvez trouver les consignes : [https://gitlab.com/pouzol4/covoit-master/-/blob/master/assets/Enonc%C3%A9.pdf](https://gitlab.com/pouzol4/covoit-master/-/blob/master/assets/Enonc%C3%A9.pdf)

## Comment utiliser le projet ?

Pour ce projet, on peut juste aller sur le lien : [http://covoit-master.hadrien-pouzol.fr/](http://covoit-master.hadrien-pouzol.fr/)

## Etat du projet 

Fini

## Langages

**FrontEnd :** HTML / CSS

**BackEnd :** PHP

## Auteurs

Hadrien POUZOL
